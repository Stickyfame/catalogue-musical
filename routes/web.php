<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MusicController;
use App\Http\Controllers\DanceController;
use App\Http\Controllers\RegionController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

//====== recherche ====

Route::get('/search', [MusicController::class, 'searchform'])->name('search');

Route::post('/search', [MusicController::class, 'search'])->name('musics.searchpost');

// ===== Musics =====
Route::get('/musics', [MusicController::class, 'index'])->name('musics.index');
Route::post('/musics', [MusicController::class, 'store'])->name('musics.store');

Route::get('/musics/create', [MusicController::class, 'create'])->name('musics.create');

Route::get('/musics/{id}/edit', [MusicController::class, 'edit'])->name('musics.edit');
Route::post('/musics/{id}/edit', [MusicController::class, 'update'])->name('musics.update');

Route::get('/musics/{id}', [MusicController::class, 'show'])->name('musics.show');
Route::post('/musics/{id}', [MusicController::class, 'destroy'])->name('musics.destroy');


// ===== Dances =====
Route::post('/dances', [DanceController::class, 'store'])->name('dances.store');

// creation d'une danse
Route::get('/dances/create', function() {
    return view('dances.create');
})->name('dances.create');

// route pour afficher toutes les musiques correspondant a la danse actuelle
Route::get('/dances/{id}', [DanceController::class, 'show'])->name('dances.musics.show');
Route::post('/dances/{id}', [DanceController::class, 'destroy'])->name('dances.destroy');


// ===== Regions =====
Route::post('/regions', [RegionController::class, 'store'])->name('regions.store');

// creation d'une region
Route::get('/regions/create', function() {
    return view('regions.create');
})->name('regions.create');

// route pour afficher toutes les musiques correspondant a la region actuelle
Route::get('/region/{id}', [RegionController::class, 'show'])->name('regions.musics.show');
Route::post('/region/{id}', [RegionController::class, 'destroy'])->name('regions.destroy');
