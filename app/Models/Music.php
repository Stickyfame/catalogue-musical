<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Music extends Model
{
    use HasFactory;

    protected $table = 'musics';
    protected $guarded = [];

    public function dance()
    {
        return $this->belongsTo('App\Models\Dance');
    }
    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    public function path()
    {
        return route('musics.show', $this);
    }

    public function delete() {
        if ($this->partition) {
            Storage::delete('public/'.$this->partition);
        }
        parent::delete();
    }

}
