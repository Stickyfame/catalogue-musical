<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Storage;
use App\Models\Music;
use App\Models\Region;
use App\Models\Dance;
use Illuminate\Support\Collection;

class MusicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        //
        return view('musics.index', [
            'musics_html' => Music::latest()->take(10)->get()
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $regions = Region::all();
        $dances = Dance::all();
        return view('musics.create', [
            'regions'=>$regions,
            'dances'=>$dances
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        //TODO: Validation du type de fichier pour pdf et mp3

        // creer la musique dans la DB
        $title = $request->input('title');
        $author = $request->input('author');
        $dance_id = $request->input('dance_id');
        $region_id = $request->input('region_id');
        $partition = $request->file('partition');
        $audio = $request->file('audio');

        $music = Music::create([
            'title'=>$title,
            'author'=>$author,
            'dance_id'=>$dance_id,
            'region_id'=>$region_id
        ]);

        if (!empty($partition)) {
            $partition_path = $music->id.'-'.$music->title.'.pdf';
        }
        else {
            $partition_path = null;
        }
        $music->partition = $partition_path;
        $music->save();

        if (!empty($partition))
            $partition->storeAs('public',$partition_path);

        if (!empty($audio)) {
            $audio_path = $music->id.'-'.$music->title.'.mp3';
        }
        else {
            $audio_path = null;
        }
        $music->audio = $audio_path;
        $music->save();

        if (!empty($audio)) {
            $audio->storeAs('public',$audio_path);
        }

        return redirect(route('musics.show', $music->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Factory|View
     */
    public function show($id)
    {
        //
        $musique = Music::findOrFail($id);
        return view('musics.show', [
            'musique_fiche'=>$musique
        ]);
    }

    /**
     * Chercher et afficher les resultats
     *
     * @param Request $request
     * @return Factory|View
     */
    public function search(Request $request)
    {
        //recup formulaire
        $author = $request->input('author');
        $dance_id = $request->input('dance_id');
        $region_id = $request->input('region_id');
        $dance_filters = $request->input('dance_filters');

        $regions = Region::all();
        $dances = Dance::all();

        if ($author) {
            $musics_author = Music::where('author',$author)->get();
        } else {
            $musics_author = Music::all();
        }

        if ($region_id) {
            $region = Region::find($region_id);
            $musics_region = $region->musics;
        } else {
            $musics_region = Music::all();
        }

        if ($dance_id) {
            $dance = Dance::find($dance_id);
            $musics_dance = $dance->musics;
        }
        else {
            if ($dance_filters == "1") {

                $is_touching = $request->input('is_touching') == "on";

                $is_exhausting = $request->input('is_exhausting') == "on";

                $is_couple = $request->input('is_couple') == "on";

                $is_switching = $request->input('is_switching') == "on";

                //$unit_nb_dancer = $request->input('unit_nb_dancer');
                //$formation = $request->input('formation');

                $dancesreq = Dance::where([
                    ['is_touching', $is_touching],
                    ['is_exhausting', $is_exhausting],
                    ['is_couple', $is_couple],
                    ['is_switch', $is_switching]
                ])->get();
                $musics_dance = collect([]);

                // Moche mais pas encore trouvé le moyen de le faire sans flag, un concat sur une collection vide marche pas
                $flag = false;
                $have_concat = false;
                foreach ($dancesreq as $une_dance) {
                    if ($flag) {
                        $musics_dance->concat($une_dance->musics);
                        $have_concat = true;
                    } else {
                        $musics_dance = $une_dance->musics;
                        $flag = true;
                    }
                }
                if ($have_concat)
                    $musics_dance = $musics_dance->collapse()->all();
                else
                    $musics_dance = $musics_dance->all();

            }
            else
                $musics_dance = Music::all();
        }
        $musics_final = $musics_region->intersect($musics_author->intersect($musics_dance))->sortBy('title')->all();

        return view('musics.search',[
            'results'=>$musics_final,
            'regions'=>$regions,
            'dances'=>$dances
            ])->withInput($request->flash());
    }

    /**
     * Show the search form
     *
     * @return View
     */
    public function searchform()
    {
        $regions = Region::all();
        $dances = Dance::all();
        return view('musics.search', [
            'regions'=>$regions,
            'dances'=>$dances
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $music = Music::findOrFail($id);
        $dances = Dance::all();
        $regions = Region::all();

        return view('musics.edit', [
            'music'=>$music,
            'dances'=>$dances,
            'regions'=>$regions
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request, $id)
    {
        $music = Music::findOrFail($id);

        //TODO: faire une fonction validateMusic et passer à ->update($request->validateMusic())
        $music->title = $request->input('title');
        $music->author = $request->input('author');
        $music->dance_id = $request->input('dance_id');
        $music->region_id = $request->input('region_id');

        //TODO: modifier le nom du PDF lors du changement de titre
        $partition = $request->file('partition');
        if (!empty($partition))
        {
            // The user wants to change the partition
            $partition_path = null;

            if ($music->partition != null)
            {
                // delete former partition and save the new one
                $partition_path = $music->partition;
                Storage::delete('public/'.$partition_path);
                $partition->storeAs('public', $partition_path);
            }
            else
            {
                // there was no former partition
                $partition_path = $music->id.'-'.$music->title.'.pdf';
                $partition->storeAs('public', $partition_path);
            }
            $music->partition = $partition_path;
        }

        $music->save();

        //TODO: modifier le nom du mp3 lors du changement de titre
        $audio = $request->file('audio');
        if (!empty($audio))
        {
            // The user wants to change the audio
            $audio_path = null;

            if ($music->audio != null)
            {
                // delete former audio and save the new one
                $audio_path = $music->audio;
                Storage::delete('public/'.$audio_path);
                $audio->storeAs('public', $audio_path);
            }
            else
            {
                // there was no former audio
                $audio_path = $music->id.'-'.$music->title.'.mp3';
                $audio->storeAs('public', $audio_path);
            }
            $music->audio = $audio_path;
        }

        $music->save();

        return redirect(route('musics.show', $music->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse|Redirector
     */
    public function destroy($id)
    {
        $music = Music::findOrFail($id);

        $music->delete();

        return redirect(route('musics.index'));
    }

    /**
     * Return all Musics in json format
     *
     * @return JsonResponse
     */
    public function apiIndex() {
        $musics = Music::all();
        foreach ($musics as $music) {
            $music->dance;
            $music->region;
        }
        return response()->json($musics);
    }
}
