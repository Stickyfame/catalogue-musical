<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Models\Music;
use App\Models\Region;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $name = $request->input('name');
        $country = $request->input('country');
        $region = Region::create([
            'name'=>$name,
            'country'=>$country
        ]);
        $region->save();

        return redirect(route('regions.musics.show', $region->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Factory|View
     */
    public function show($id)
    {
        //
        $region = Region::findOrFail($id);
        $musiques = $region->musics;
        return view('regions.show', [
            'musiques'=>$musiques,
            'region'=>$region
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function destroy($id)
    {
        $region = Region::findOrFail($id);

        $musics = $region->musics;
        foreach ($musics as $music) {
            $music->delete();
        }
        $region->delete();

        return redirect(route('musics.index'));
    }

    /**
     * Return all Regions in json format
     *
     * @return JsonResponse
     */
    public function apiIndex(): JsonResponse
    {
        $regions = Region::all();
        return response()->json($regions);
    }
}
