<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Models\Music;
use App\Models\Dance;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

class DanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:dances'
        ]);

        $name = $request->input('name');
        if ($request->input('is_couple') == "on") {
            $is_couple = true;
        } else {
            $is_couple = false;
        }
        $formation = $request->input('formation');
        if ($request->input('is_touching') == "on") {
            $is_touching = true;
        } else {
            $is_touching = false;
        }
        if ($request->input('is_switching') == "on") {
            $is_switching = true;
        } else {
            $is_switching = false;
        }
        if ($request->input('is_exhausting') == "on") {
            $is_exhausting = true;
        } else {
            $is_exhausting = false;
        }

        $unit_nb_dancer = $request->input('unit_nb_dancer');

        $dance = Dance::create([
            'name'=>$name,
            'is_couple'=>$is_couple,
            'formation'=>$formation,
            'is_touching'=>$is_touching,
            'is_switch'=>$is_switching,
            'unit_nb_dancer'=>$unit_nb_dancer,
            'is_exhausting' =>$is_exhausting
        ]);
        $dance->save();
        return redirect(route('dances.musics.show', $dance->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Factory|View
     */
    public function show($id)
    {
        //
        $dance = Dance::findOrFail($id);
        $musiques = $dance->musics;
        return view('dances.show', [
            'musiques'=>$musiques,
            'dance'=>$dance
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function destroy($id)
    {
        $dance = Dance::findOrFail($id);

        $musics = $dance->musics;
        foreach ($musics as $music) {
            $music->delete();
        }
        $dance->delete();
        return redirect(route('musics.index'));
    }

    /**
     * Return all Dances in json format
     *
     * @return JsonResponse
     */
    public function apiIndex() {
        $dances = Dance::all();
        return response()->json($dances);
    }
}
