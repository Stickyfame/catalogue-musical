<p align="center"><img src="storage/app/public/fox.png" width="200" alt="logo"></p>

<p align="center">
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# Folk Finder

Cette application web a pour but de faciliter la recherce et la constitution d'un recueil de partitions de musiques Folk. Les différentes fonctionnalités suivantes sont disponibles :

- Chercher une musique par danse, compositeur, région ou des filtres spécifiques
- Afficher toutes les musiques d'un type de danse (Ex: Scottish)
- Afficher toutes les musiques d'une région (Ex: Bretagne)
- Ajouter facilement des musiques, ainsi que la partition au format PDF
- Afficher une musique avec toutes ses informations et sa partition

> Cette application est prévue pour être utilisée localement sur un réseau privé. Elle ne convient pas (encore) à une utilisation publique sur internet.

## Technologie

L'application utilise Laravel comme framework PHP (backend), et une base de donnée locale en SQLite.

Il est prévu de modifier l'interface utilisateur avec un framework JS (React).

## License

FolkFinder is open-sourced project licensed under the [MIT license](https://opensource.org/licenses/MIT).
