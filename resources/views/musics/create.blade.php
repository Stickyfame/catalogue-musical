@extends('layouts.common')

@section('title', 'Ajouter une musique' )

@section('content')

<div class="text-center">
    <h3>Ajouter une nouvelle musique</h3>
        <form action="{{ route('musics.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <label for="title-form">Titre de la musique</label>
            <input class="mb-1" type="text" name="title" id="title-form" placeholder="Ex : Zelda" required>

            <label for="author-form">Compositeur</label>
            <input class="mb-1" type="text" name="author" id="author-form" placeholder="Ex : Philippe Plard">

            <label for="dance-id">Type de danse</label>
            <select className="dance-id" name="dance_id" placeholder="Choose one" required id="dance-id">
                @foreach ($dances as $dance)
                <option value="{{$dance->id}}">{{$dance->name}}</option>
                @endforeach
            </select>

            <label for="region-id">Region</label>
            <select className="region-id" name="region_id" placeholder="Choose one" required id="region-id">
                @foreach ($regions as $region)
                    <option value="{{$region->id}}">{{$region->name}}</option>
                @endforeach

            </select>

            <label for="partition-id">Partition</label>
            <input type="file" name="partition" id="partition-id">

            <labe for="audio-id">Audio</labe>
            <input type="file" name="audio" id="partition-id">

            <button class="btn-primary mt-2" type="submit">Enregistrer</button>
            </form>
</div>
@endsection
