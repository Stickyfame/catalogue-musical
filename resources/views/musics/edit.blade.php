@extends('layouts.common')

@section('title', 'Modifier une musique' )

@section('content')

<div class="text-center">
    <h3>Modifier "{{$music->title}}"</h3>
        <form action="{{ route('musics.update', $music->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <label for="title">Titre de la musique</label>
            <input class="mb-1" type="text" name="title" id="title-form" value="{{$music->title}}" required>

            <label for="author">Compositeur</label>
            <input class="mb-1" type="text" name="author" id="author-form" value="{{$music->author}}">

            <label for="dance-id">Type de danse</label>
            <select className="dance-id" name="dance_id" placeholder="Choose one" required>
                @foreach ($dances as $dance)
                    @if($dance->id == $music->dance->id)
                        <option value="{{$dance->id}}" selected>{{$dance->name}}</option>
                    @else
                        <option value="{{$dance->id}}">{{$dance->name}}</option>
                    @endif
                @endforeach
            </select>

            <label for="region-id">Region</label>
            <select className="region-id" name="region_id" placeholder="Choose one" required>
                @foreach ($regions as $region)
                    @if($region->id == $music->region->id)
                        <option value="{{$region->id}}" selected>{{$region->name}}</option>
                    @else
                        <option value="{{$region->id}}">{{$region->name}}</option>
                    @endif
                @endforeach

            </select>

            @if($music->partition)
                <label for="currentPartition">Partition actuelle</label>
                <object data="{{Storage::url('public/'.$music->partition)}}" type="application/pdf" style="width:100%; height:500px;"></object>
            @endif
            <label for="partition-id">@if($music->partition) Modifier la partition @else Ajouter une partition @endif</label>
            <input type="file" name="partition" id="partition-id" accept="application/pdf">

            <label for="audio-id">@if($music->audio) Modifier l'audio @else Ajouter un audio @endif</label>
            <input type="file" name="audio" id="audio-id" accept="application/mpeg4-generic, .mp3">

            <button class="btn-primary mt-2" type="submit">Enregistrer</button>
        </form>
</div>
@endsection
