@extends('layouts.common')

@section('title', 'Musiques')

@section('content')

<div class="text-center">
    <h3>Voici les musiques récemment ajoutées :</h3>
    <div class="divider"></div>

    <ul class="menu">

        <li class="menu-item selected">
            <ul class="menu u-overflow-auto">
                @foreach ($musics_html as $une_musique)
                    <li class="menu-item"><a style="font-size: large;" href="{{ $une_musique->path() }}">{{ $une_musique->title }} - {{ $une_musique->dance->name }}</a></li>
                @endforeach
            </ul>
        </li>

    </ul>


 </div>


@endsection
