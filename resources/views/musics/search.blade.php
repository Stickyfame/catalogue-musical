@extends('layouts.common')

@section('title', 'Chercher' )

@section('content')

<div class="text-center">
    <h3>Chercher une musique</h3>
    <form action="{{ route('musics.searchpost') }}" method="POST">
        @csrf

        <label for="author">Auteur</label>
        <input class="mb-1" type="text" name="author" id="title-form" placeholder="Ex : Ciac Boum" value="{{old('author')}}">

        <label for="dance-id">Type de danse</label>
        <select className="dance-id" name="dance_id" placeholder="Choose one">
            <option value="">Toutes les danses</option>
            @foreach ($dances as $dance)
            <option value="{{$dance->id}}" {{ old("dance_id") == $dance->id ? "selected":"" }}>{{$dance->name}}</option>
            @endforeach
        </select>

        <label for="region-id">Region</label>
        <select className="region-id" name="region_id" placeholder="Choose one">
            <option value="">Toutes les régions</option>
            @foreach ($regions as $region)
                <option value="{{$region->id}}" {{ old("region_id") == $region->id ? "selected":"" }}>{{$region->name}}</option>
            @endforeach
        </select>

        <input type="hidden" id="dance-filters" name="dance_filters" value="{{old('dance_filters')}}">

        <button class="btn-secondary mt-2" onclick="switchFilter()" type="button" id="switch-button">Filtres de danses</button>

        <div id="filters-container" style="display: none">
            <div class="form-ext-control">
                <label class="form-ext-toggle__label"><span>Les danseurs.ses se touchent</span>
                    <div class="form-ext-toggle">
                        <input name="is_touching" type="checkbox" class="form-ext-input" {{old("is_touching") ? "checked" : ""}} />
                        <div class="form-ext-toggle__toggler"><i></i></div>
                    </div>
                </label>
            </div>

            <div class="form-ext-control">
                <label class="form-ext-toggle__label"><span>La danse est fatiguante</span>
                    <div class="form-ext-toggle">
                        <input name="is_exhausting" type="checkbox" class="form-ext-input" {{old("is_exhausting") ? "checked" : ""}} />
                        <div class="form-ext-toggle__toggler"><i></i></div>
                    </div>
                </label>
            </div>

            <div class="form-ext-control">
                <label class="form-ext-toggle__label"><span>Danse de couple</span>
                    <div class="form-ext-toggle">
                        <input name="is_couple" type="checkbox" class="form-ext-input" {{old("is_couple") ? "checked" : ""}} />
                        <div class="form-ext-toggle__toggler"><i></i></div>
                    </div>
                </label>
            </div>

            <div class="form-ext-control">
                <label class="form-ext-toggle__label"><span>Changement de partenaire</span>
                    <div class="form-ext-toggle">
                        <input name="is_switching" type="checkbox" class="form-ext-input" {{old("is_switching") ? "checked" : ""}} />
                        <div class="form-ext-toggle__toggler"><i></i></div>
                    </div>
                </label>
            </div>
        </div>
        <script type="text/javascript">
            let switcher = document.getElementById("dance-filters");
            let container = document.getElementById("filters-container");
            let button = document.getElementById("switch-button");

            if (switcher.value === "1") {
                container.style["display"] = "block";
                button.innerHTML = "Retirer les filtres de danses";
            }
            else {
                container.style["display"] = "none";
                button.innerHTML = "Ajouter des filtres de danses";
            }

            function switchFilter() {
                let switcher = document.getElementById("dance-filters");
                let container = document.getElementById("filters-container");
                let button = document.getElementById("switch-button");

                if (switcher.value === "") switcher.value = 0;
                switcher.value = 1 - switcher.value;
                console.log(switcher.value);
                if (switcher.value === "1") {
                    container.style["display"] = "block";
                    button.innerHTML = "Retirer les filtres de danses";
                }
                else {
                    container.style["display"] = "none";
                    button.innerHTML = "Ajouter des filtres de danses";
                }
            }
        </script>

            <!--label for="unit_nb_dancer">Nombre de danseur.ses pour entrer dans la danse :</label><br>
            <input class="mb-1" type="number" name="unit_nb_dancer" id="unit_nb_dancer-form" min="1"><br>

            <label for="formation">Formation de la danse</label>
            <input class="mb-1" type="text" name="formation" id="formation-form" placeholder="Ex : Cercle / Ligne"><br-->

        <button class="btn-primary mt-2" type="submit">Chercher</button>
    </form>
    <hr>
    @isset($results)
    @if (count($results))
        <h3>{{count($results)}} résultat(s) </h3>
        <ul>
        @foreach ($results as $music)
        <li><a href="{{$music->path()}}">{{$music->title}} - {{$music->author}} - {{$music->dance->name}}</a></li>
        @endforeach
        </ul>
    @else
        Pas de resultats
    @endif
    @endisset
</div>
@endsection
