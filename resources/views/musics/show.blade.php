@extends('layouts.common')

@section('title', $musique_fiche->title )

@section('content')

    <h3 class="u-text-center">{{ $musique_fiche->title }}</h3>
    <div class="divider"></div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-container">
                    <div class="card-image" style="background-color:rebeccapurple;background-image:radial-gradient(
                                    circle at top right,
                                    rgba(0, 255, 255, 1), rgba(0, 255, 255, 0)
                                  ),
                                  radial-gradient(
                                    circle at bottom left,
                                    rgba(255, 20, 146, 1), rgba(255, 20, 146, 0)
                                  )"></div>
                    <div class="title-container">
                        <p class="title">{{$musique_fiche->title}}</p>
                        <span class="subtitle">
                            @if($musique_fiche->author == "")
                                Inconnu
                            @else
                                {{$musique_fiche->author}}
                            @endif
                        </span>
                    </div>
                </div>
                <div class="content">
                    <ul>
                        <li>Danse : <a href="{{ $musique_fiche->dance->path() }}">{{ $musique_fiche->dance->name }}</a></li>
                        <li>Region : <a href="{{ $musique_fiche->region->path()}}">{{ $musique_fiche->region->name }}</a></li>
                        @if($musique_fiche->partition)<li><a target="_blank" href="{{Storage::url('public/'.$musique_fiche->partition)}}">Voir la partition</a></li>@endif
                        @if($musique_fiche->audio)
                            <li><a target="_blank" href="{{Storage::url('public/'.$musique_fiche->audio)}}">Telecharger l'audio</a></li>
                            <audio
                                controls
                                src="{{Storage::url('public/'.$musique_fiche->audio)}}">
                                Ce navigateur ne supporte pas l'<code>audio</code>.
                            </audio>
                        @endif
                    </ul>
                </div>
                <div class="action-bar u-center">
                    <a class="btn btn-link" href="{{route('musics.edit', $musique_fiche->id)}}">Modifier cette musique</a>
                </div>
                <div class="card-footer">

                    <div class="modal modal-animated--zoom-in" id="delete-modal">
                        <a href="#searchModalDialog" class="modal-overlay close-btn" aria-label="Close"></a>
                        <div class="modal-content" role="document">
                            <div class="modal-header"><a href="#components" class="u-pull-right" aria-label="Close"><span class="icon"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11 fa-wrapper" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg></span></a>
                                <div class="modal-title">Suppression</div>
                            </div>
                            <div class="modal-body">
                                <div class="r">
                                    <h3 class="font-alt font-light u-text-center">Êtes-vous sûr.e de vouloir supprimer  "{{ $musique_fiche->title }}" ?</h3></div>
                                <div class="space"></div>
                                <div class="input-control">
                                    <form action="{{route('musics.destroy', $musique_fiche->id)}}" method="post" class="u-center">
                                        @csrf
                                        <button class="outline btn-primary" type="submit"><strong>Supprimer définitevement</strong></button>
                                    </form>
                                </div>
                                <div class="divider"></div>

                            </div>

                        </div>
                    </div>

                    <a href="#delete-modal"><div class="u-text-center btn btn-primary mx-3">Supprimer  <i class="fas fa-trash-alt"></i></div></a>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            @if($musique_fiche->partition)
                <object data="{{Storage::url('public/'.$musique_fiche->partition)}}" type="application/pdf" style="width:100%; height: 100%;"></object>
            @else
                <p>La partition de cette musique n'a pas été ajoutée.</p>
            @endif
        </div>
    </div>


@endsection
