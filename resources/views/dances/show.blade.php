@extends('layouts.common')

@section('title', $dance->name )

@section('content')


            <h3 class="u-text-center">{{ $dance->name }}</h3>
            <div class="divider"></div>
            <div class="row">
                <div class="col-sm-6">

                    <div class="card">
                        <div class="card-container">
                            <div class="card-image" style="background-color:rebeccapurple;background-image:radial-gradient(
                                    circle at top right,
                                    rgba(0, 255, 255, 1), rgba(0, 255, 255, 0)
                                  ),
                                  radial-gradient(
                                    circle at bottom left,
                                    rgba(255, 20, 146, 1), rgba(255, 20, 146, 0)
                                  )"></div>
                            <div class="title-container">
                                <p class="title">Fiche récapitulative</p><span class="subtitle">{{$dance->name}}</span></div>
                        </div>
                        <div class="content">
                            <p>Informations :</p>
                            @if ($dance->is_couple)
                                <i class="fas fa-wrapper fa-user-friends mr-1"></i>Danse de couple
                            @else
                                <i class="fas fa-wrapper fa-spinner mr-1"></i>Formation : {{$dance->formation}}
                            @endif
                            <br>
                            @if ($dance->is_switch)
                                <i class="fas fa-wrapper fa-sync-alt mr-1"></i>Avec changement de partenaires
                            @else
                                <i class="fas fa-wrapper fa-ban mr-1"></i>Sans changement de partenaires
                            @endif
                            <br>
                            @if ($dance->is_touching)
                                <i class="fas fa-wrapper fa-virus mr-1"></i>Cette danse n'est pas Covid-Friendly :(
                            @else
                                <i class="fas fa-wrapper fa-virus mr-1"></i>Cette danse est Covid-Friendly :)
                            @endif
                            <br>
                            @if ($dance->unit_nb_dancer == 1)
                                <i class="fas fa-wrapper fa-street-view mr-1"></i>Vous dansez seul
                            @else
                                <i class="fas fa-wrapper fa-user-plus mr-1"></i>Vous dansez a {{$dance->unit_nb_dancer}} personnes
                            @endif
                            <br>
                            @if ($dance->is_exhausting)
                                <i class="fas fa-wrapper fa-tint mr-1"></i>Cette danse est fatiguante
                            @else
                                <i class="fas fa-wrapper fa-smile mr-1"></i>Cette danse est reposante
                            @endif
                        </div>
                        <div class="action-bar u-center">
                            <a class="btn btn-link" href="">Modifier cette danse</a>
                        </div>
                        <div class="card-footer">

                            <div class="modal modal-animated--zoom-in" id="delete-modal">
                                <a href="#searchModalDialog" class="modal-overlay close-btn" aria-label="Close"></a>
                                <div class="modal-content" role="document">
                                    <div class="modal-header"><a href="#components" class="u-pull-right" aria-label="Close"><span class="icon"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11 fa-wrapper" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg></span></a>
                                        <div class="modal-title">Suppression</div>
                                    </div>
                                    <div class="modal-body">
                                        <div class="r">
                                            <h3 class="font-alt font-light u-text-center">Êtes-vous sûr.e de vouloir supprimer  "{{ $dance->name }}" ?</h3>
                                            <p class="u-center">Cela entrainera la suppression de {{$musiques->count()}} musique(s).</p>
                                        </div>
                                        <div class="space"></div>
                                        <div class="input-control">
                                            <form action="{{route('dances.destroy', $dance->id)}}" method="post" class="u-center">
                                                @csrf
                                                <button class="outline btn-primary" type="submit"><strong>Supprimer définitevement</strong></button>
                                            </form>
                                        </div>
                                        <div class="divider"></div>

                                    </div>

                                </div>
                            </div>

                            <a href="#delete-modal"><div class="u-text-center btn btn-primary mx-3">Supprimer  <i class="fas fa-trash-alt"></i></div></a>
                        </div>
                    </div>


                </div>
                <div class="col-sm-6">
                    <h5>Les musiques de {{$dance->name}}</h5>
                    <div class="divider"></div>

                    <ul class="menu" style="position: relative;">

                        <li class="menu-item selected">
                            <ul class="menu u-overflow-auto" style="max-height: 500px;">
                                <li class="menu-item"><span style="font-size: large"> Liste des musiques </span></li>
                                @foreach ($musiques as $une_musique)
                                    <li class="menu-item"><a style="font-size: large;" href="{{ $une_musique->path() }}">{{ $une_musique->title }}</a></li>
                                @endforeach
                                <li class="menu-item"><span style="font-size: large">Pas d'autres musiques correspondantes</span></li>
                            </ul>
                        </li>
                        <div style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; pointer-events: none; background: linear-gradient(0deg, rgba(255,255,255,1) 0%, rgba(255,255,255,0) 50%, rgba(255,255,255,1) 100%); ">
                        </div>
                    </ul>

                </div>
            </div>
@endsection
