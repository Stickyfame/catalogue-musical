@extends('layouts.common')

@section('title', 'Ajouter une danse' )
    
@section('content')

<div class="text-center">
    <h3>Ajouter une nouvelle danse</h3>
        <form action="{{ route('dances.store') }}" method="POST">
            @csrf
               
                <label for="name">Nom de la danse</label>
                <input type="text" name="name" id="name-form" placeholder="Ex : Scottish" value="{{old('name')}}" required>
                @error('name')
                <span class="info text-red-700">Erreur : {{$message}}</span>
                @enderror
                <label for="formation">Formation de la danse</label>
                <input class="mb-1" type="text" name="formation" id="formation-form" placeholder="Ex : Cercle / Ligne" required><br>

                <label for="unit_nb_dancer">Nombre de danseur.ses pour entrer dans la danse :</label><br>
                <input class="mb-1" type="number" name="unit_nb_dancer" id="unit_nb_dancer-form" min="1" required><br>
                
                <div class="form-ext-control">
                    <label class="form-ext-toggle__label"><span>C'est une danse de couple</span>
                        <div class="form-ext-toggle">
                            <input name="is_couple" type="checkbox" class="form-ext-input" />
                            <div class="form-ext-toggle__toggler"><i></i></div>
                        </div>
                    </label>
                </div>

                <div class="form-ext-control">
                    <label class="form-ext-toggle__label"><span>Il y a changement de partenaire</span>
                        <div class="form-ext-toggle">
                            <input name="is_switching" type="checkbox" class="form-ext-input" />
                            <div class="form-ext-toggle__toggler"><i></i></div>
                        </div>
                    </label>
                </div>

                <div class="form-ext-control">
                    <label class="form-ext-toggle__label"><span>Les danseurs.ses se touchent</span>
                        <div class="form-ext-toggle">
                            <input name="is_touching" type="checkbox" class="form-ext-input" />
                            <div class="form-ext-toggle__toggler"><i></i></div>
                        </div>
                    </label>
                </div>

                <div class="form-ext-control">
                    <label class="form-ext-toggle__label"><span>La danse est fatiguante</span>
                        <div class="form-ext-toggle">
                            <input name="is_exhausting" type="checkbox" class="form-ext-input" />
                            <div class="form-ext-toggle__toggler"><i></i></div>
                        </div>
                    </label>
                </div>

                <button class="btn-primary mt-2" type="submit">Enregistrer</button>
            </form>
</div>
@endsection