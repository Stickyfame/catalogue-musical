@extends('layouts.common')

@section('title', $region->name )

@section('content')

<div class="text-center">
    <h3>Toutes les danses de "{{ $region->name }}"</h3>

    <div class="modal modal-animated--zoom-in" id="delete-modal">
        <a href="#searchModalDialog" class="modal-overlay close-btn" aria-label="Close"></a>
        <div class="modal-content" role="document">
            <div class="modal-header"><a href="#components" class="u-pull-right" aria-label="Close"><span class="icon"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11 fa-wrapper" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg></span></a>
                <div class="modal-title">Suppression</div>
            </div>
            <div class="modal-body">
                <div class="r">
                    <h3 class="font-alt font-light u-text-center">Êtes-vous sûr.e de vouloir supprimer  "{{ $region->name }}" ?</h3>
                    <p class="u-center">Cela entrainera la suppression de {{$musiques->count()}} musique(s).</p>
                </div>
                <div class="space"></div>
                <div class="input-control">
                    <form action="{{route('regions.destroy', $region->id)}}" method="post" class="u-center">
                        @csrf
                        <button class="outline btn-primary" type="submit"><strong>Supprimer définitevement</strong></button>
                    </form>
                </div>
                <div class="divider"></div>

            </div>

        </div>
    </div>

    <a class="btn btn-primary" href="#delete-modal">Supprimer <i class="fas fa-trash-alt"></i></a>
        <ul>
            @foreach ($musiques as $une_musique)
            <li><a href="{{$une_musique->path()}}">{{ $une_musique->title }}</a></li>
            @endforeach
        </ul>
</div>
@endsection
