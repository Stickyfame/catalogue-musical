@extends('layouts.common')

@section('title', 'Ajouter une region' )
    
@section('content')

<div class="text-center">
    <h3>Ajouter une nouvelle region</h3>
        <form action="{{ route('regions.store') }}" method="POST">
            @csrf
                <label for="name">Nom de la region</label>
                <input class="mb-1" type="text" name="name" id="name-form" placeholder="Ex : Bretagne" required>

                <label for="formation">Pays de la region</label>
                <input class="mb-1" type="text" name="country" id="formation-form" placeholder="Ex : France" required>

                <button class="btn-primary mt-2" type="submit">Enregistrer</button>
            </form>
</div>
@endsection