<!DOCTYPE html>
<html id="page" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>FolkFinder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge;"/>
    <link href="https://unpkg.com/cirrus-ui" type="text/css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">          <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous" ></script>
</head>
<body>
<div class="header header-fixed unselectable header-animated">
    <div class="header-brand">
        <div class="nav-item no-hover">
            <a href="{{route('home')}}"><h6 class="title">FolkFinder</h6></a>
        </div>
        <div class="nav-item nav-btn" id="header-btn"> <span></span> <span></span> <span></span> </div>
    </div>
    <div class="header-nav" id="header-menu">
        <div class="nav-left">

        </div>
        <div class="nav-right">
            <div class="nav-item">
                <a href="{{ url('/musics')}}"><i class="fas fa-wrapper fa-music mr-1"></i>Musiques récentes</a>
            </div>
            <div class="nav-item">
                <a href="{{route('musics.create')}}"><i class="fas fa-wrapper fa-plus mr-1"></i>Ajouter une musique</a>
            </div>
            <div class="nav-item">
                <a href="{{route('search')}}"><i class="fas fa-wrapper fa-search mr-1"></i>Rechercher une musique</a>
            </div>
            <div class="nav-item">
                <a href="{{ route('dances.create') }}"><i class="fas fa-wrapper fa-plus mr-1"></i>Ajouter une danse</a>
            </div>
            <div class="nav-item">
                <a href="{{ route('regions.create') }}"><i class="fas fa-wrapper fa-plus mr-1"></i>Ajouter une region</a>
            </div>
        </div>
    </div>
</div>
<section class="section">
    <div class="hero fullscreen">
        <div class="hero-body">
            <div class="content">

                <div class="text-center">
                    <h1>👋 Bienvenue dans le catalogue de vos musiques Folk!</h1>
                    <h6 class="font-alt font-light">Pour ne plus perdre de temps à trouver quoi jouer en bal. By <b>Toffie & ToffieBrother.</b></h6>
                    <div class="space large"></div>
                    <div class="u-center">
                        <a href="{{route('search')}}" class="btn btn-primary btn--pilled">Trouver une musique<i class="fa-wrapper fa fa-search pad-left"></i></a>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<script>
    // Get all the nav-btns in the page
    let navBtns = document.querySelectorAll('.nav-btn');

    // Add an event handler for all nav-btns to enable the dropdown functionality
    navBtns.forEach(function (ele) {
        ele.addEventListener('click', function() {
            // Get the dropdown-menu associated with this nav button (insert the id of your menu)
            let dropDownMenu = document.getElementById('header-menu');

            // Toggle the nav-btn and the dropdown menu
            ele.classList.toggle('active');
            dropDownMenu.classList.toggle('active');
        });
    });
</script>
</body>
</html>

